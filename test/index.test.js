const { MAX_HISTORY } = require('../src/constants')
const generateTroops = require('../src/index')

const mockHelpers = jest.requireActual('../src/helpers')

jest.mock('../src/helpers', () => ({
  ...jest.requireActual('../src/helpers'),
  randomNumber: jest.fn().mockImplementation((min, max) => mockHelpers.randomNumber(min, max))
}))

describe('generate troops', () => {
  it('should generate different troops', () => {
    expect(generateTroops()).not.toEqual(generateTroops())
  })

  it('should not generate equal troops for the maximum history span', () => {
    for (let i = 0; i < 100; i += 1) {
      const troopsKeys = []

      for (let j = 0; j < MAX_HISTORY; j += 1) {
        const troops = generateTroops()
        const troopsKey = Object.values(troops).join('-')

        expect(troopsKeys).not.toContain(troopsKey)

        troopsKeys.push(troopsKey)
      }
    }
  }, 120000)

  it('should fail on too many retries', () => {
    expect(() => {
      let counter = 100000
      while (counter > 0) {
        counter -= 1
        generateTroops({ retry: 1, maxRetries: -1 })
      }
    }).toThrow('random troops generation failed')
  })
})
