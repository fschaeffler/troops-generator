const { unique } = require('underscore')
const { sortObjectByKey, randomNumber, shuffleArray } = require('../src/helpers')

describe('sort object by key', () => {
  it('should sort an object by key', () => {
    const input = { a: 1, c: 2, b: 3 }
    const output = { a: 1, b: 3, c: 2 }

    expect(sortObjectByKey(input)).toEqual(output)
  })
})

describe('random number', () => {
  it('should throw an error on too small minimum', () => {
    expect(() => randomNumber(-1, 10)).toThrow('max and min must be postive integers')
  })

  it('should throw an error on incorrect minimum type', () => {
    expect(() => randomNumber('abc', 10)).toThrow('max and min must be postive integers')
  })

  it('should throw an error on incorrect maximum type', () => {
    expect(() => randomNumber('abc', 10)).toThrow('max and min must be postive integers')
  })

  it('should throw an error on smaller maximum than minimum', () => {
    expect(() => randomNumber(10, 5)).toThrow('max needs to be bigger than min')
  })

  it('should not fail on one-step range', () => {
    expect(randomNumber(10, 10)).toEqual(10)
  })

  it('should create a random number in range', () => {
    const result = randomNumber(5, 10)

    expect(result).toBeGreaterThanOrEqual(5)
    expect(result).toBeLessThanOrEqual(10)
  })

  it('should create a random number with undefined max', () => {
    const result = randomNumber(5)

    expect(result).toBeGreaterThanOrEqual(5)
    expect(result).toBeLessThanOrEqual(Number.MAX_SAFE_INTEGER)
  })

  it('should create a random number with undefined min', () => {
    const result = randomNumber(undefined, 5)

    expect(result).toBeGreaterThanOrEqual(0)
    expect(result).toBeLessThanOrEqual(5)
  })
})

describe('randomize sorting', () => {
  it('should randomize not in place if defined', () => {
    const input = ['abc', 'def', 'ghi', 'jkl', 'mno']
    const inputClone = JSON.parse(JSON.stringify(input))

    expect(shuffleArray(input, false)).not.toEqual(input)
    expect(shuffleArray(input, true)).toEqual(input)
    expect(shuffleArray(input, true)).not.toEqual(inputClone)
  })

  it('should randomize a sorting', () => {
    const input = ['abc', 'def', 'ghi', 'jkl', 'mno']
    const inputClone = JSON.parse(JSON.stringify(input))

    expect(shuffleArray(inputClone)).not.toEqual(input)
  })

  it('should have random last entries', () => {
    const input = ['abc', 'def', 'ghi', 'jkl', 'mno']
    const inputClone = JSON.parse(JSON.stringify(input))

    const outputs = [
      shuffleArray(inputClone)[inputClone.length - 1],
      shuffleArray(inputClone)[inputClone.length - 1],
      shuffleArray(inputClone)[inputClone.length - 1],
      shuffleArray(inputClone)[inputClone.length - 1],
      shuffleArray(inputClone)[inputClone.length - 1]
    ]

    expect(unique(outputs)).not.toHaveLength(1)
  })

  it('should contain all entries', () => {
    const input = ['abc', 'def', 'ghi', 'jkl', 'mno']
    const inputClone = JSON.parse(JSON.stringify(input))
    const output = shuffleArray(inputClone)

    expect(output).toHaveLength(input.length)
  })
})
