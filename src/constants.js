const NO_OF_UNITS = 167
const MAX_RETRIES = 5
const MAX_HISTORY = 100

const UNIT_TYPES = {
  ARCHER: 'archer',
  SPEARMAN: 'spearman',
  SWORDSMAN: 'swordsman'
}

const UNIT_TYPE_VALUES = Object.values(UNIT_TYPES)

module.exports = {
  NO_OF_UNITS,
  UNIT_TYPES,
  UNIT_TYPE_VALUES,
  MAX_RETRIES,
  MAX_HISTORY
}
