#!/usr/bin/env node

const generateTroops = require('./index')
const { NO_OF_UNITS, UNIT_TYPE_VALUES } = require('./constants')

const noOfUnits = parseInt(process.argv[2] || NO_OF_UNITS, 10)

if (Number.isNaN(noOfUnits) || noOfUnits < UNIT_TYPE_VALUES.length) {
  throw new Error(`incorrect number of units - ${process.argv[2]}`)
}

const troops = generateTroops({ noOfUnits })

// eslint-disable-next-line no-console
console.log(`Input: ${noOfUnits} units\n Result: ${JSON.stringify(troops, null, 2)}`)
