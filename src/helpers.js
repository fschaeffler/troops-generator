const randomNumber = (min = 0, max = Number.MAX_SAFE_INTEGER) => {
  if (min < 0 || typeof min !== 'number' || typeof max !== 'number') {
    throw new Error('max and min must be postive integers')
  }

  if (min === max) {
    return min
  }

  if (min > max) {
    throw new Error('max needs to be bigger than min')
  }

  return Math.floor(Math.random() * (max - min) + min)
}

const shuffleArray = (values, inPlace = true) => {
  const result = inPlace ? values : JSON.parse(JSON.stringify(values))

  let counter = result.length
  let index
  let temp

  while (counter > 0) {
    index = Math.floor(Math.random() * counter)

    counter -= 1

    temp = result[counter]
    result[counter] = result[index]
    result[index] = temp
  }

  return result
}

const sortObjectByKey = (input) =>
  Object.keys(input)
    .sort()
    .reduce(
      (memory, key) => ({
        ...memory,
        [key]: input[key]
      }),
      {}
    )

module.exports = {
  randomNumber,
  shuffleArray,
  sortObjectByKey
}
