const { NO_OF_UNITS, UNIT_TYPE_VALUES, MAX_RETRIES, MAX_HISTORY } = require('./constants')
const { shuffleArray, sortObjectByKey, randomNumber } = require('./helpers')

let round = 0
const history = []

const isHistoryUnique = (troops) => {
  const troopsKey = Object.values(troops).join('-')
  return !history.includes(troopsKey)
}

/**
 * @typedef Troop
 * @property {number} archer Number of archers
 * @property {number} spearman Number of spearmen
 * @property {number} [scope] Number of swordsmen
 */

/**
 * Generate random distribution of troop with different unit types.
 *
 * @param {object} params
 * @param {number} [params.noOfUnits=NO_OF_UNITS] Number of how man units should get generated.
 * @param {number} [params.retry=0] Current retry counter value.
 * @param {number} [params.maxRetries=MAX_RETRIES] Maximum retries when troops structure already exists.
 * @returns {Troop} Troop structure
 * @example
 * const generateTroops = require('@fschaeffler/troops-generator')
 * const troops = generateTroops()
 * console.log('Troops', JSON.stringify(troops, null, 2))
 */
const generateTroops = ({ noOfUnits = NO_OF_UNITS, retry = 0, maxRetries = MAX_RETRIES } = {}) => {
  round += retry === 0 ? 1 : 0

  let total = 0

  const troops = {}

  const randomizedUnitTypes = shuffleArray(UNIT_TYPE_VALUES)

  randomizedUnitTypes.forEach((unitType, index) => {
    if (index === randomizedUnitTypes.length - 1) {
      troops[unitType] = noOfUnits - total
      total += troops[unitType]
      return
    }

    const max = noOfUnits - total - UNIT_TYPE_VALUES.length + index
    const units = randomNumber(1, max)

    troops[unitType] = units
    total += units
  })

  const sortedTroops = sortObjectByKey(troops)

  if (!isHistoryUnique(sortedTroops)) {
    if (retry > maxRetries) {
      throw new Error(`random troops generation failed after ${retry}. retry`)
    }

    return generateTroops(noOfUnits, retry + 1)
  }

  delete troops.total

  history[(round - 1) % MAX_HISTORY] = Object.values(sortedTroops).join('-')

  return sortedTroops
}

module.exports = generateTroops
