# Troops Generator

## Local Development Environment

- install NVM: https://github.com/nvm-sh/nvm#installing-and-updating
- use the correct Node.js version: `nvm use`
- [if necessary] install the correct Node.js version via `nvm install` and run `nvm use` again
- install dependencies: `npm ci`

## Test

The test can get started via `npm run test`. The output contains if the test run did fail or succeed, as well as a test coverage.

## Usage (NPX)

The troops generator can get used without the need for an installation. This can get done via `npx https://gitlab.com/fschaeffler/troops-generator`.

## Usage (CLI)

The NPM package offers a global executable called `troops-generator`. In order to use this executable a global installation is needed via `npm install https://gitlab.com/fschaeffler/troops-generator`.

**Example:**

```
flo@bull:~/dev/public/troops-generator$ npm install -g https://gitlab.com/fschaeffler/troops-generator
/home/flo/.nvm/versions/node/v14.16.1/bin/troops-generator -> /home/flo/.nvm/versions/node/v14.16.1/lib/node_modules/@fschaeffler/troops-generator/src/cli.js
+ @fschaeffler/troops-generator@1.0.0
added 1 package from 1 contributor in 18.341s

flo@bull:~/dev/public/troops-generator$ troops-generator
Input: 167 units
Result: {
  "archer": 20,
  "spearman": 12,
  "swordsman": 135
}

flo@bull:~/dev/public/troops-generator$ troops-generator 100
Input: 100 units
Result: {
  "archer": 82,
  "spearman": 9,
  "swordsman": 9
}
```

## Usage (Programmatically)

**Example:**

```
const generateTroops = require('@fschaeffler/troops-generator')
const troops = generateTroops()
console.log('Troops', JSON.stringify(troops, null, 2))
```

**API:**

```
/**
 * @typedef Troop
 * @property {number} archer Number of archers
 * @property {number} spearman Number of spearmen
 * @property {number} [scope] Number of swordsmen
 */

/**
 * Generate random distribution of troop with different unit types.
 * 
 * @param {object} params
 * @param {number} [params.noOfUnits=NO_OF_UNITS] Number of how man units should get generated.
 * @param {number} [params.retry=0] Current retry counter value.
 * @param {number} [params.maxRetries=MAX_RETRIES] Maximum retries when troops structure already exists.
 * @returns {Troop} Troop structure
 * @example
 * const generateTroops = require('@fschaeffler/troops-generator')
 * const troops = generateTroops()
 * console.log('Troops', JSON.stringify(troops, null, 2))
 */
```
